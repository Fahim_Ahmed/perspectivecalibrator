A simple utility for projection mapping and live augmented reality setup.

Dependency
----------
* [ofxMtlMapping2D](https://github.com/morethanlogic/ofxMtlMapping2D)
* [ofxPostProcessing](https://github.com/neilmendoza/ofxPostProcessing)
* [ofxUI](http://github.com/rezaali/ofxUI)
* ofxAssimpModelLoader
* ofxXmlSettings