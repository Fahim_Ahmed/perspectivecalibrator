#pragma once

#include "ofMain.h"
#include "ofxUI.h"
#include "ofxAssimpModelLoader.h"

class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		void exit();
		

	private:
		videoInput				vInput;
		ofImage					canvas;
		ofImage					img;
		ofImage					screenImage;
		ofImage					shadowImage;
		ofVideoPlayer			video;
		ofxUICanvas				*gui;	
		ofxAssimpModelLoader	model;
		ofLight					light;		

		void processOpenedFile(ofFileDialogResult result, int type = 0);
		void initGui();
		void guiEvent(ofxUIEventArgs &e);
		void paintSlider(ofxUISlider &slider, int precision);
		void openImage(int type = 0);
		void saveScreen(ofImage &image);
};
