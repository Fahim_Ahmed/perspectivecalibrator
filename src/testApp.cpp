#include "testApp.h"

#define UI_WIDGET_WIDTH 180
#define UI_WIDGET_HEIGHT 15

#define COLOR_DARK_GRAY_BLUE 30, 33, 46
#define COLOR_GRAY_BLUE 67, 72, 89
#define COLOR_DARK_RED 176, 26, 51
#define COLOR_DARK_BLUE 18, 77, 190

int device = 0, vw, vh, newFrameCount = 0, iAlpha = 255, gAlpha = 180, adjustY = 0, vAlpha = 255;
unsigned char *buffer;
double elasped;
float xRotation, iScale = 0.5, mScale = 0.75, vScale = 1, vxRotation = 0, ixRotation = 0;
bool isLoaded = false, showModel = true, showImage = true, showGrid = true, showVideo = true;

//--------------------------------------------------------------
void testApp::setup(){
	ofSetFrameRate(30);
	ofSetVerticalSync(true);
	ofEnableAlphaBlending();
	
	initGui();

	//video.setPlayer(ofPtr<ofQTKitPlayer>(new ofQTKitPlayer));
	video.setPixelFormat(OF_PIXELS_RGBA);

	vInput.setIdealFramerate(device, 60);
	vInput.setupDevice(device, 1280, 720, VI_COMPOSITE);

	vw 	= vInput.getWidth(device);
	vh 	= vInput.getHeight(device);
	int size = vInput.getSize(device);

	canvas.allocate(vw, vh, OF_IMAGE_COLOR);
	buffer = new unsigned char[size];

	//vInput.showSettingsWindow(device);

	model.loadModel("sample.dae");
	//model.setPosition(ofGetWidth() * 0.5, (float)ofGetHeight() * 0.75 , 0);
	model.setLoopStateForAllAnimations(OF_LOOP_NORMAL);
	model.playAllAnimations();

	light.enable();
	ofEnableSeparateSpecularLight();

	shadowImage.loadImage("shadow.png");
}

//--------------------------------------------------------------
void testApp::update(){
	elasped += ofGetLastFrameTime();
	if(elasped >= 1.0){
		elasped = 0.0;

		ofSetWindowTitle("WEBCAM FPS: " +  ofToString(newFrameCount));
		newFrameCount = 0;
	}

	if(vInput.isFrameNew(device)){
		vInput.getPixels(device, buffer, true, true);
		canvas.setFromPixels(buffer, vw, vh, OF_IMAGE_COLOR);

		newFrameCount++;
	}

	if(showModel) model.update();

	if(video.isLoaded() && showVideo){
		video.update();
	}
}


//--------------------------------------------------------------
void testApp::draw(){
	int sw = ofGetWidth();
	int sh = ofGetHeight();

	ofDisableLighting();
	ofSetColor(255);
	canvas.draw(0,0);
	
	ofPushMatrix();
	ofTranslate(sw * 0.5, sh * 0.5);
	ofRotateX(xRotation);

	if(isLoaded && showImage){
		int w = img.getWidth();
		int h = img.getHeight();		

		w *= iScale;
		h *= iScale;

		ofPushMatrix();
		//ofTranslate(sw * 0.5, sh * 0.5);
		ofRotateX(ixRotation);
		ofSetColor(255, iAlpha);

		img.draw( -w * 0.5 , -h * 0.5, w, h);

		ofPopMatrix();		
	}
	
	if(showVideo && video.isLoaded()){
		int vw = video.getWidth();
		int vh = video.getHeight();

		vw *= vScale;
		vh *= vScale;

		ofPushMatrix();
		//ofTranslate(sw * 0.5, sh * 0.5);
		ofRotateX(vxRotation);
		ofSetColor(255, vAlpha);
		video.draw( -vw * 0.5 , -vh * 0.5, vw, vh);
		ofPopMatrix();
	}

	if(showGrid){
		ofPushMatrix();	
		ofTranslate(0, adjustY);
		//ofTranslate(sw * 0.5, sh * 0.5 + adjustY);
		//ofRotateX(xRotation);
		ofRotate(90, 0, 1, 0);
		ofSetColor(255, gAlpha);
		ofDrawGridPlane(sw>>1, 12, false);
		ofPopMatrix();
	}

	if(showModel){
		ofEnableLighting();

		ofEnableDepthTest();
		ofSetColor(255);

		ofPushMatrix();
		//ofTranslate(sw * 0.5, sh * 0.5);
		//ofRotateX(xRotation);
		//shadowImage.draw( -shadowImage.getWidth() * 0.5, -shadowImage.getHeight() * 0.5);
		shadowImage.draw(-82, -44, 164, 164);
		ofPopMatrix();

		ofPushMatrix();
		//ofTranslate(sw * 0.5, sh * 0.5);
		ofScale( mScale, mScale, mScale);
		//ofRotateX(xRotation - 90);
		ofRotateX(- 90);
		model.drawFaces();

		ofPopMatrix();
		
		ofDisableDepthTest();
	}

	ofPopMatrix();	
}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){
	if(key == ' '){
		openImage();
	}

	if(key == 'h'){
		gui->toggleVisible();
	}
}

void testApp::mouseMoved(int x, int y ){

}
void testApp::mouseDragged(int x, int y, int button){

}
void testApp::mousePressed(int x, int y, int button){

}
void testApp::mouseReleased(int x, int y, int button){

}
void testApp::windowResized(int w, int h){

}
void testApp::gotMessage(ofMessage msg){

}
void testApp::dragEvent(ofDragInfo dragInfo){ 

}
void testApp::exit() {
	vInput.stopDevice(device);
	gui->saveSettings("guiSettings.xml");
	delete gui;	
}

void testApp::initGui(){
	gui = new ofxUICanvas(211, ofGetHeight());
	ofAddListener(gui->newGUIEvent, this, &testApp::guiEvent); 

	gui->setFont("visitor2.ttf");
	gui->setFontSize(OFX_UI_FONT_MEDIUM, 6);

	gui->addFPS();

	ofxUILabelButton *lb = gui->addLabelButton("Load Image", false, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	lb->setColorBack(ofxUIColor(COLOR_DARK_RED, 220));

	ofxUILabelButton *lvb = gui->addLabelButton("Load Video", false, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	lvb->setColorBack(ofxUIColor(COLOR_DARK_RED, 220));
	
	ofxUILabelButton *wb = gui->addLabelButton("Open webcam config", false, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	wb->setColorBack(ofxUIColor(COLOR_DARK_RED, 220));

	gui->addSpacer();
	ofxUIMinimalSlider *s = gui->addMinimalSlider("Scale", 0.0f, 1.0f, iScale, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*s, 2);

	ofxUIMinimalSlider *vs = gui->addMinimalSlider("Video Scale", 0.0f, 1.0f, vScale, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*vs, 2);

	ofxUIMinimalSlider *ms = gui->addMinimalSlider("Model Scale", 0.0f, 1.0f, mScale, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*ms, 2);

	gui->addSpacer();
	ofxUIMinimalSlider *rx = gui->addMinimalSlider("World Rotate X", 0, 90, xRotation, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*rx, 2);		

	ofxUIMinimalSlider *irx = gui->addMinimalSlider("Image Rotate X", -90, 90, ixRotation, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*irx, 2);	

	ofxUIMinimalSlider *vrx = gui->addMinimalSlider("Video Rotate X", -90, 90, vxRotation, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*vrx, 2);

	gui->addSpacer();
	ofxUIMinimalSlider *as = gui->addMinimalSlider("Alpha", 0, 255, iAlpha, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*as, 0);

	ofxUIMinimalSlider *vas = gui->addMinimalSlider("Video Alpha", 0, 255, vAlpha, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*vas, 0);

	ofxUIMinimalSlider *ag = gui->addMinimalSlider("Grid Alpha", 0, 255, gAlpha, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	paintSlider(*ag, 0);

	gui->addSpacer();
	ofxUILabelToggle *t = gui->addLabelToggle("Image Toggle", showImage, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	t->setColorBack(ofxUIColor(COLOR_DARK_GRAY_BLUE, 255));
	t->setColorFill(ofxUIColor(COLOR_GRAY_BLUE, 255));

	ofxUILabelToggle *vt = gui->addLabelToggle("Video Toggle", showVideo, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	vt->setColorBack(ofxUIColor(COLOR_DARK_GRAY_BLUE, 255));
	vt->setColorFill(ofxUIColor(COLOR_GRAY_BLUE, 255));

	ofxUILabelToggle *t2 = gui->addLabelToggle("Model Toggle", showModel, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	t2->setColorBack(ofxUIColor(COLOR_DARK_GRAY_BLUE, 255));
	t2->setColorFill(ofxUIColor(COLOR_GRAY_BLUE, 255));

	ofxUILabelToggle *t3 = gui->addLabelToggle("Grid Toggle", showGrid, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	t3->setColorBack(ofxUIColor(COLOR_DARK_GRAY_BLUE, 255));
	t3->setColorFill(ofxUIColor(COLOR_GRAY_BLUE, 255));

	gui->addSpacer();
	ofxUILabelButton *pb = gui->addLabelButton("Pause Video", false, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT * 2);
	pb->setColorBack(ofxUIColor(COLOR_DARK_BLUE, 220));

	ofxUILabelButton *sb = gui->addLabelButton("Screenshot", false, UI_WIDGET_WIDTH, UI_WIDGET_HEIGHT);
	sb->setColorBack(ofxUIColor(COLOR_DARK_RED, 220));

	gui->addLabel("hints", "Press H to toggle GUI");
	
	gui->loadSettings("guiSettings.xml");	
}


void testApp::guiEvent(ofxUIEventArgs &e){
	ofxUISlider *slider = e.getSlider();

	if(e.getName() == "Load Image"){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;
		if(!btn->getValue()) openImage(0);
	}else if(e.getName() == "Load Video"){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;
		if(!btn->getValue()) openImage(1);
	}else if(e.getName() == "Open webcam config"){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;
		if(!btn->getValue()) vInput.showSettingsWindow(device);
	}else if(e.getName() == "World Rotate X"){
		xRotation = slider->getScaledValue();
	}else if(e.getName() == "Image Rotate X"){
		ixRotation = slider->getScaledValue();
	}else if(e.getName() == "Video Rotate X"){
		vxRotation = slider->getScaledValue();
	}else if(e.getName() == "Scale"){
		iScale = slider->getScaledValue();
	}else if(e.getName() == "Alpha"){
		iAlpha = slider->getScaledValue();
	}else if(e.getName() == "Grid Alpha"){
		gAlpha = slider->getScaledValue();
	}else if(e.getName() == "Model Scale"){
		mScale = slider->getScaledValue();
	}else if(e.getName() == "Image Toggle"){
		ofxUIToggle *toggle = (ofxUIToggle *) e.getToggle();
		showImage = toggle->getValue();
	}else if(e.getName() == "Model Toggle"){
		ofxUIToggle *toggle = (ofxUIToggle *) e.getToggle();
		showModel = toggle->getValue();
	}else if(e.getName() == "Grid Toggle"){
		ofxUIToggle *toggle = (ofxUIToggle *) e.getToggle();
		showGrid = toggle->getValue();
	}else if(e.getName() == "Screenshot"){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;
		if(!btn->getValue()) saveScreen(screenImage);
	} if(e.getName() == "Video Scale"){
		vScale = slider->getScaledValue();
	}else if(e.getName() == "Video Alpha"){
		vAlpha = slider->getScaledValue();
	}else if(e.getName() == "Video Toggle"){
		ofxUIToggle *toggle = (ofxUIToggle *) e.getToggle();
		showVideo = toggle->getValue();
	}else if(e.getName() == "Pause Video"){
		ofxUILabelButton *btn = (ofxUILabelButton *) e.widget;
		if(!btn->getValue()) video.setPaused(!video.isPaused());
	}
}

void testApp::paintSlider(ofxUISlider &slider, int precision){
	slider.setLabelPrecision(precision);
	slider.setColorFill(ofxUIColor(COLOR_GRAY_BLUE, 255));
	slider.setColorBack(ofxUIColor(COLOR_DARK_GRAY_BLUE, 255));
}

void testApp::openImage(int type){
	ofFileDialogResult openFileResult = type == 0 ? ofSystemLoadDialog("Select a jpg or png") : ofSystemLoadDialog("Select a mov file");

	if(openFileResult.bSuccess) {
		processOpenedFile(openFileResult, type);
	} else {
		//isLoaded = false;
	}
}

void testApp::processOpenedFile(ofFileDialogResult result, int type){
	ofFile file(result.getPath());

	if(file.exists()){
		string fileExtension = ofToUpper(file.getExtension());

		if ((fileExtension == "JPG" || fileExtension == "PNG") && type == 0) {
			img.loadImage(result.getPath());
			isLoaded = true;			
			/*img.setImageType(OF_IMAGE_GRAYSCALE);

			bool isPortrait = img.width >= img.height ? false : true;
			float r;
			int newW, newH;

			if(isPortrait){
				r = (float) img.height / (float) img.width;
				newW = (int) (scale / r);
				newH = scale;
			}else{
				r = (float) img.width / (float)img.height;
				newW = scale;
				newH = (int) (scale / r);		
			}

			img.resize(newW, newH);
			sw = img.width;
			sh = img.height;

			featuresFinder(img);*/
		}else if (type == 1) {			
			video.loadMovie(result.getPath());
			video.play();
		//	cout << result.getPath() << endl;
		}
	}
}

void testApp::saveScreen(ofImage &image) {	
	image.grabScreen(0,0, ofGetWidth(), ofGetHeight());
	
	string fileName = "snapshot_"+ofGetTimestampString()+".png";
	image.saveImage(fileName);
}